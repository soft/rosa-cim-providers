Name:           rosa-cim-providers
Version:        0.1
Release:        3.5
License:        LGPLv2+
Summary:        Set of basic CIM providers for ROSA Linux
Group:          System Environment/Libraries
URL:            http://www.rosalab.ru
Source0:        %{name}-%{version}.tar.xz

%global providers_version %{version}-%{release}
%global logfile %{_localstatedir}/log/rosa-cim-install.log
%global required_konkret_ver 0.9.0-2
%global required_libuser_ver 0.60
%define _libexecdir %{_prefix}/libexec

BuildRequires:  cmake
BuildRequires:  konkretcmpi-devel >= %{required_konkret_ver}
BuildRequires:  sblim-cmpi-devel
BuildRequires:  cim-schema
BuildRequires:  cim-schema
BuildRequires:  python
BuildRequires:  pkgconfig(libsemanage)
BuildRequires:  libglib2-devel

BuildRequires:  setools-devel

# for lmi account
BuildRequires:  libuser-devel >= %{required_libuser_ver}
BuildRequires:  python-devel

# sblim-sfcb or tog-pegasus
# (required to be present during install/uninstall for registration)
# sblim_sfcb not supported yet
Requires:       tog-pegasus
Requires(pre):  tog-pegasus
Requires(preun): tog-pegasus
Requires(post): tog-pegasus
Requires:       python

Requires:       libsemanage
Requires:       libsepol

# for lmi account
Requires:       libuser >= %{required_libuser_ver}
Provides:       cura-account = %{providers_version_release}
Obsoletes:      cura-account < 0.0.10-1

%description
%{name} is set of (usually) small CMPI providers (agents) for basic
monitoring and management of host system using Common Information
Model (CIM).


%prep
%setup -q

%build
%{cmake}
make -k %{?_smp_mflags} all


%install
make install/fast DESTDIR=$RPM_BUILD_ROOT -C build

# The log file must be created
mkdir -p "$RPM_BUILD_ROOT/%{_localstatedir}/log"
touch "$RPM_BUILD_ROOT/%logfile"


%files
%doc README COPYING
%dir %{_sysconfdir}/rosa-cim
%config(noreplace) %{_sysconfdir}/rosa-cim/rosa-cim.conf
%attr(755, root, root) %{_bindir}/rosa-cim-register
%attr(755, root, root) %{_libexecdir}/pegasus/*-cimprovagt
%{_libdir}/librosacimcommon.so.*
%{_libdir}/librosacimindmanager.so*
%{_libdir}/cmpi/libcmpi*.so
%{_datadir}/%{name}/*.mof
%{_datadir}/%{name}/cmpiRLMI*.reg
%{_datadir}/%{name}/cmpiLMI*.reg

%ghost %logfile

%pre
# If upgrading, deregister old version
if [ "$1" -gt 1 ]; then
    %{_bindir}/rosa-cim-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/00_RLMI_Test.mof \
        %{_datadir}/%{name}/cmpiRLMI_Test.reg || :;

    %{_bindir}/rosa-cim-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_RLMI_Fan.mof \
        %{_datadir}/%{name}/cmpiRLMI_Fan.reg;

    %{_bindir}/rosa-cim-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_RLMI_SELinux.mof \
        %{_datadir}/%{name}/cmpiRLMI_SELinux.reg || :;

    %{_bindir}/rosa-cim-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_RLMI_License.mof \
        %{_datadir}/%{name}/cmpiRLMI_License.reg || :;

    %{_bindir}/rosa-cim-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_LMI_Locale.mof \
        %{_datadir}/%{name}/cmpiLMI_Locale.reg || :;

    %{_bindir}/rosa-cim-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_RLMI_Locale.mof \
        %{_datadir}/%{name}/cmpiRLMI_Locale.reg || :;

    %{_bindir}/rosa-cim-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_RLMI_TimeZone.mof \
        %{_datadir}/%{name}/cmpiRLMI_TimeZone.reg || :;

    %{_bindir}/rosa-cim-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_RLMI_Account.mof \
        %{_datadir}/%{name}/cmpiRLMI_Account.reg || :;

    %{_bindir}/rosa-cim-register -n root/interop -c tog-pegasus \
        delete-pgmodule RLMITestModule FanModule SELinuxUsersModule RLMILicenseModule \
         LocaleModule RLMILocaleModule RLMITimeZoneModule RLMIAccountModule
fi

%post
/sbin/ldconfig
# Register Schema and Provider
if [ "$1" -ge 1 ]; then
    %{_bindir}/rosa-cim-register -v %{providers_version} -n root/interop -c tog-pegasus \
        --just-mofs register %{_datadir}/%{name}/RLMITestModule_Providers.mof;
    %{_bindir}/rosa-cim-register -v %{providers_version} -n root/interop -c tog-pegasus \
        --just-mofs register %{_datadir}/%{name}/FanModule_Providers.mof;
    %{_bindir}/rosa-cim-register -v %{providers_version} -n root/interop -c tog-pegasus \
        --just-mofs register %{_datadir}/%{name}/SELinuxUsersModule_Providers.mof;
    %{_bindir}/rosa-cim-register -v %{providers_version} -n root/interop -c tog-pegasus \
        --just-mofs register %{_datadir}/%{name}/RLMILicenseModule_Providers.mof;
    %{_bindir}/rosa-cim-register -v %{providers_version} -n root/interop -c tog-pegasus \
        --just-mofs register %{_datadir}/%{name}/LocaleModule_Providers.mof;
    %{_bindir}/rosa-cim-register -v %{providers_version} -n root/interop -c tog-pegasus \
        --just-mofs register %{_datadir}/%{name}/RLMILocaleModule_Providers.mof;
    %{_bindir}/rosa-cim-register -v %{providers_version} -n root/interop -c tog-pegasus \
        --just-mofs register %{_datadir}/%{name}/RLMITimeZoneModule_Providers.mof;
    %{_bindir}/rosa-cim-register -v %{providers_version} -n root/interop -c tog-pegasus \
        --just-mofs register %{_datadir}/%{name}/RLMIAccountModule_Providers.mof;

    %{_bindir}/rosa-cim-register -v %{providers_version} register \
        %{_datadir}/%{name}/00_RLMI_Test.mof \
        %{_datadir}/%{name}/cmpiRLMI_Test.reg;

    %{_bindir}/rosa-cim-register -v %{providers_version} register \
        %{_datadir}/%{name}/60_RLMI_Fan.mof \
        %{_datadir}/%{name}/cmpiRLMI_Fan.reg;

    %{_bindir}/rosa-cim-register -v %{providers_version} -n root/interop -c tog-pegasus \
        --just-mofs register \
        %{_datadir}/%{name}/90_RLMI_Fan_Profile.mof;

    %{_bindir}/rosa-cim-register -v %{providers_version} register \
        %{_datadir}/%{name}/60_RLMI_SELinux.mof \
        %{_datadir}/%{name}/cmpiRLMI_SELinux.reg;

    %{_bindir}/rosa-cim-register -v %{providers_version} register \
        %{_datadir}/%{name}/60_RLMI_License.mof \
        %{_datadir}/%{name}/cmpiRLMI_License.reg;

    %{_bindir}/rosa-cim-register -v %{providers_version} register \
        %{_datadir}/%{name}/60_LMI_Locale.mof \
        %{_datadir}/%{name}/cmpiLMI_Locale.reg;

    %{_bindir}/rosa-cim-register -v %{providers_version} register \
        %{_datadir}/%{name}/60_RLMI_Locale.mof \
        %{_datadir}/%{name}/cmpiRLMI_Locale.reg;

    %{_bindir}/rosa-cim-register -v %{providers_version} register \
        %{_datadir}/%{name}/60_RLMI_TimeZone.mof \
        %{_datadir}/%{name}/cmpiRLMI_TimeZone.reg;

    %{_bindir}/rosa-cim-register -v %{providers_version} register \
        %{_datadir}/%{name}/60_RLMI_Account.mof \
        %{_datadir}/%{name}/cmpiRLMI_Account.reg;
fi

%preun
# Deregister only if not upgrading
if [ "$1" -eq 0 ]; then
    %{_bindir}/rosa-cim-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/00_RLMI_Test.mof \
        %{_datadir}/%{name}/cmpiRLMI_Test.reg || :;

    %{_bindir}/rosa-cim-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_RLMI_Fan.mof \
        %{_datadir}/%{name}/cmpiRLMI_Fan.reg || :;

    %{_bindir}/rosa-cim-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_RLMI_SELinux.mof \
        %{_datadir}/%{name}/cmpiRLMI_SELinux.reg || :;

    %{_bindir}/rosa-cim-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_RLMI_License.mof \
        %{_datadir}/%{name}/cmpiRLMI_License.reg || :;

    %{_bindir}/rosa-cim-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_RLMI_TimeZone.mof \
        %{_datadir}/%{name}/cmpiRLMI_TimeZone.reg || :;

    %{_bindir}/rosa-cim-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_RLMI_Account.mof \
        %{_datadir}/%{name}/cmpiRLMI_Account.reg || :;

    %{_bindir}/rosa-cim-register -n root/interop -c tog-pegasus \
        delete-pgmodule RLMITestModule FanModule SELinuxUsersModule RLMILicenseModule \
         LocaleModule RLMILocaleModule RLMITimeZoneModule RLMIAccountModule
fi

%postun -p /sbin/ldconfig



%changelog
* Mon Oct 06 2014 Kuzma Kazygashev <kuzma.kazygashev@rosalab.ru> 0.1-3.4
- Add RLMI_Account providers version 0.4.3.

* Mon Sep 24 2014 Alexander Lakhin <a.lahin@ntcit-rosa.ru> - 0.1-3
- Add LMI_Locale, RLMI_Locale, and RLMI_TimeZone providers

* Mon Sep 18 2014 Alexander Lakhin <a.lahin@ntcit-rosa.ru> - 0.1-2
- Add RLMI_License provider

* Mon Sep 8 2014 Alexander Lakhin <a.lahin@ntcit-rosa.ru> - 0.1-1
- Initial package
